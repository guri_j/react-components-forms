import React, { Fragment } from "react";
import ReactDOM from "react-dom/client";
import Header from "./components/Header"; // Correct paths to components
import Main from "./components/Main";
import Footer from "./components/Footer";
import "./index.css";
const App = ({ children }) => {
  return (
    <Fragment>
      <Header title="Header title" />
      <Main />
      <Footer title="Footer title" />
      {children}
    </Fragment>
  );
};
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

export default App;
