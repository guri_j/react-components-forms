import React from "react";
import PropTypes from "prop-types";
import "../index.css";

const Footer = ({ title }) => {
  return (
    <footer>
      <p>{title}</p>
    </footer>
  );
};

Footer.propTypes = {
  title: PropTypes.string,
};

export default Footer;
